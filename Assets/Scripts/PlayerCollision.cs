﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
	public int nbCoins = 0;
	public GameObject pickupEffect;
	public GameObject mobEffect;
	private bool isInstantiate = true;

	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "coin")
		{
			GameObject go = Instantiate(pickupEffect, other.transform.position, Quaternion.identity);
			Destroy(go, 0.5f);
			nbCoins++;
			Destroy(other.gameObject);
		}
	}

	private void OnControllerColliderHit(ControllerColliderHit collision)
	{
		if(collision.gameObject.tag == "ennemi")
		{
			print("touché !!!");
		}

		if(collision.gameObject.tag == "mob" && isInstantiate)
		{
			//je saute sur le monte
			isInstantiate=false;
			GameObject go = Instantiate (mobEffect, collision.transform.position, Quaternion.identity);
			Destroy(go, 0.5f);
			Destroy(collision.gameObject.transform.parent.gameObject, 0.5f);
			StartCoroutine("ResetInstantiate");
			
		}

		IEnumerator ResetInstantiate(){
			yield return new WaitForSeconds(.8f);
			isInstantiate = true;
		}

	}



}
