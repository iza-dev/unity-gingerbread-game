﻿// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;
// using System.Linq;


// public class EnnemiAI : MonoBehaviour
// {

// 	private float _attackRange = 3f;
//     private float _rayDistance = 5.0f;
//     private float _stoppingDistance = 1.5f;

//     private Vector3 _destination;
//     private Quaternion _desiredRotation;
//     private Vector3 _direction;
//     //le joueur pain depices
//     private EnnemiAI _target;

//     //les differents etats de l'ennemi
//     private EnnemiAIState _currentState;


//     //On vérifie les différents états de l'ennemi(escargot) et en fonction son comportement change.

// 	private void Update(){
// 		switch (_currentState)
//         {
//         	//se trompe
//         	case EnnemiAIState.Wander:
//             {
//                 if (NeedsDestination())
//                 {
//                     GetDestination();
//                 }

//                 transform.rotation = _desiredRotation;

//                 transform.Translate(Vector3.forward * Time.deltaTime * 5f);

//                 var rayColor = Color.red ;
//                 Debug.DrawRay(transform.position, _direction * _rayDistance, rayColor);

//                 while (IsPathBlocked())
//                 {
//                     Debug.Log("Path Blocked");
//                     GetDestination();
//                 }

//                 var targetToAggro = CheckForAggro();
//                 if (targetToAggro != null)
//                 {
//                     _target = targetToAggro.GetComponent<EnnemiAI>();
//                     _currentState = EnnemiAIState.Chase;
//                 }
                
//                 break;
//             }

//         	//chasse
//         	case EnnemiAIState.Chase:
//         	{
//         		if (_target == null)
//                 {
//                    _currentState = EnnemiAIState.Wander;
//                 	print("target is null");
//                     return;
//                 }
//                  //on se rapproche
//                 transform.LookAt(_target.transform);
//                 transform.Translate(Vector3.forward * Time.deltaTime * 5f);

//                 //on vérifie si on peu passer à l'attaque en fonction de notre position
//                 if (Vector3.Distance(transform.position, _target.transform.position) < _attackRange)
//                 {
//                     _currentState = EnnemiAIState.Attack;
//                 }
//                 break;
//         	}

//         	//attaque
//         	case EnnemiAIState.Attack:
//             {
//                 if (_target != null)
//                 {
//                     Destroy(_target.gameObject);
//                     print("target is null");
//                 }
                
//                 _currentState = EnnemiAIState.Wander;
//                 break;
//             }
//         }
// 	}

// 	    private bool IsPathBlocked()
//     {
//        // Ray ray = new Ray(transform.position, _direction);
//         //var hitSomething = Physics.RaycastAll(ray, _rayDistance, _layerMask);
//         return hitSomething.Any();
//     }

// 	 private void GetDestination()
//     {
//         Vector3 testPosition = (transform.position + (transform.forward * 4f)) +
//                                new Vector3(UnityEngine.Random.Range(-4.5f, 4.5f), 0f,
//                                    UnityEngine.Random.Range(-4.5f, 4.5f));

//         _destination = new Vector3(testPosition.x, 1f, testPosition.z);

//         _direction = Vector3.Normalize(_destination - transform.position);
//         _direction = new Vector3(_direction.x, 0f, _direction.z);
//         _desiredRotation = Quaternion.LookRotation(_direction);
//     }

//     private bool NeedsDestination()
//     {
//         if (_destination == Vector3.zero)
//             return true;

//         var distance = Vector3.Distance(transform.position, _destination);
//         if (distance <= _stoppingDistance)
//         {
//             return true;
//         }

//         return false;
//     }

//     Quaternion startingAngle = Quaternion.AngleAxis(-60, Vector3.up);
//     Quaternion stepAngle = Quaternion.AngleAxis(5, Vector3.up);

//      private Transform CheckForAggro()
//     {
//         float aggroRadius = 5f;
        
//         RaycastHit hit;
//         var angle = transform.rotation * startingAngle;
//         var direction = angle * Vector3.forward;
//         var pos = transform.position;
//         for(var i = 0; i < 24; i++)
//         {
//             if(Physics.Raycast(pos, direction, out hit, aggroRadius))
//             {
//                 var drone = hit.collider.GetComponent<EnnemiAI>();
//                 if(drone != null && drone.Team != gameObject.GetComponent<EnnemiAI>().Team)
//                 {
//                     Debug.DrawRay(pos, direction * hit.distance, Color.red);
//                     return drone.transform;
//                 }
//                 else
//                 {
//                     Debug.DrawRay(pos, direction * hit.distance, Color.yellow);
//                 }
//             }
//             else
//             {
//                 Debug.DrawRay(pos, direction * aggroRadius, Color.white);
//             }
//             direction = stepAngle * direction;
//         }

//         return null;
//     }
// }


// public enum EnnemiAIState
// {
// 	Wander,
//     Chase,
//     Attack
// }