﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharaController : MonoBehaviour
{
	private CharacterController cc;
	public float moveSpeed;
	public float jumpForce;
	public float gravity;
	private Vector3 moveDir;
    private Animator anim;
    private bool iswalking = false;

    void Start()
    {
        cc = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
    	//calcul de la direction
        moveDir = new Vector3(Input.GetAxis("Horizontal")* moveSpeed, moveDir.y, Input.GetAxis("Vertical")* moveSpeed);
       
       //detection d'un saut sur la touche espace
        if(Input.GetButtonDown("Jump")&& cc.isGrounded)
        {
        	moveDir.y= jumpForce;
        }
        //on applique une garvitée avec une valeur négative sur l'axe y
        moveDir.y -= gravity * Time.deltaTime;

        //Si on se déplace 
        if(moveDir.x != 0 || moveDir.z != 0)
        {
            iswalking= true;
        	//le personnage se dirige vers la bonne direction
        	transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(new Vector3(moveDir.x, 0 , moveDir.z)), 0.15f);
        }
        else{
            iswalking= false;
        }

        anim.SetBool("iswalking", iswalking);

        cc.Move(moveDir*Time.deltaTime);
    }
}
